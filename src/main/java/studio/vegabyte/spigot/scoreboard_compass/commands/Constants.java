package studio.vegabyte.spigot.scoreboard_compass.commands;

public class Constants {
  static public final String base = "scoreboard-compass";

  static public final String give = "give";

  static public final String track = "track";
}
