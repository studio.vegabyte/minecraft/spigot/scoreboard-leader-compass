package studio.vegabyte.spigot.scoreboard_compass.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;

import studio.vegabyte.spigot.scoreboard_compass.App;
import studio.vegabyte.spigot.scoreboard_compass.items.CompassItem;

public class GiveCommands implements CommandExecutor {

  // #region methods

  private boolean give(CommandSender sender, Command command, String label, String[] subargs) {

    // check permission
    if (sender.hasPermission("scoreboard-compass.give")) {
      sender.sendMessage("No permission.");
      return true;
    }

    // get player
    // TODO: better target resolution
    String argPlayer = subargs[0];
    Player playerRecipient = Bukkit.getPlayerExact(argPlayer);
    if (playerRecipient == null) {
      sender.sendMessage("§C§l(!) §eInvalid player.");

      return true;
    }

    // get objective
    String argObjective = subargs[1];
    Objective objective = Bukkit.getScoreboardManager().getMainScoreboard().getObjective(argObjective);
    if (objective == null) {
      sender.sendMessage(String.format("§C§l(!) §eInvalid objective, '%d'.", argObjective));
    }

    // give new instance
    playerRecipient.getInventory().addItem(this.compassItem.Instantiate(objective));

    sender.sendMessage("§e§l(!) §eGave player compass.");

    return true;
  }

  // #endregion

  // #region handlers

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

    // ignore commands not for this plugin
    if (!command.getName().equalsIgnoreCase(Constants.base))
      return true;

    // error on improper commands
    if (args.length < 2) {
      sender.sendMessage("§C§l(!) §eInvalid usage.");
      return true;
    }

    // determine sub arguments
    String subcommand = args[0];
    String[] subArgs = Arrays.copyOfRange(args, 1, args.length);

    // exeucte subcommand
    switch (subcommand) {
      case Constants.give:
        return give(sender, command, label, subArgs);
      default:
        sender.sendMessage(String.format("§C§l(!) §eInvalid subcommand '%s'.", subcommand));
        return true;
    }
  }

  // #endregion

  // #region lifecycle
  private App app;
  private CompassItem compassItem;

  public GiveCommands(App app, CompassItem compassItem) {
    this.app = app;
    this.compassItem = compassItem;

    this.app.getCommand(Constants.base).setExecutor(this); // register commands
  }

  // #endregion
}