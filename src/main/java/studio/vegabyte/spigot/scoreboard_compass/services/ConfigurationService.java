package studio.vegabyte.spigot.scoreboard_compass.services;

import org.bukkit.configuration.file.FileConfiguration;

import studio.vegabyte.spigot.scoreboard_compass.App;

public class ConfigurationService {

  /**
   * Is the compass craftable?
   */
  public boolean IsCraftable() {
    return this.config.getBoolean("craftable");
  }

  /**
   * Get the tracked objective for the craftable compass.
   */
  public String CraftableObjective() {
    return this.config.getString("craftableObjective");
  }

  // #region Dependencies

  private App app;
  private FileConfiguration config;

  public ConfigurationService(App app) {
    this.app = app;

    this.config = app.getConfig();
    config.addDefault("craftable", false);
    config.addDefault("craftableObjective", "level");
    config.options().copyDefaults(true);
    this.app.saveConfig();
  }

  // #endregion
}
